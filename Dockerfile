FROM maven:3-jdk-8-alpine AS build

WORKDIR /usr/app/
COPY pom.xml /usr/app
COPY src /usr/app/src
RUN mvn install
EXPOSE 80
CMD [ "sh", "-c", "java -jar /usr/app/target/bootexample-0.0.1-SNAPSHOT.jar" ]



